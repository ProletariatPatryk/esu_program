﻿using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    [SerializeField]
    float targetedRotationSpeed = 20f;

    /*void OnMouseDrag()
    {
        float rotX = Input.GetAxis("Mouse X") * targetedRotationSpeed * Mathf.Deg2Rad;
        float rotY = Input.GetAxis("Mouse Y") * targetedRotationSpeed * Mathf.Deg2Rad;

        transform.Rotate(Vector3.up, -rotX);
        transform.Rotate(Vector3.right, rotY);
    }*/

    private void Update()
    {
        float rot = targetedRotationSpeed * Mathf.Deg2Rad;
        transform.Rotate(Vector3.up, rot);
    }
}